# apollo php 客户端

## 介绍


## 安装教程
```shell script
composer require dreamcat/apollo-php
```

## 使用说明

fpm 示例代码
```php
<?php
use Dreamcat\ApolloPhp\Api\Impl\ApolloServer;
use Dreamcat\ApolloPhp\Impl\ApolloReadProcess;
use Dreamcat\ApolloPhp\Impl\Saver\SingleSaver;
use Psr\Log\NullLogger;

# 名空间对应的保存文件
$map = [
    "application" => "config.php",
    "script" => "script.php",
];
$process = new ApolloReadProcess("apollo.cae");
$process->setLogger(new NullLogger())
    ->setApolloServer(new ApolloServer("http://config.url.com"))
    ->setConfigSaver(new SingleSaver($map))
    ->readConfig("appid", "cluserName", array_keys($map));

```

swoole 示例代码
```php
<?php
use Dreamcat\ApolloPhp\Api\Impl\ApolloServer;
use Dreamcat\ApolloPhp\Impl\ApolloReadProcess;
use Dreamcat\ApolloPhp\Impl\Saver\SingleSaver;
use Dreamcat\ApolloPhp\Impl\Saver\SwooleSaver;
use Psr\Log\NullLogger;

# 名空间对应的保存文件
$map = [
    "application" => "config.php",
    "script" => "script.php",
];
$process = new ApolloReadProcess("apollo.cae");
$process->setLogger(new NullLogger())
    ->setApolloServer(new ApolloServer("http://config.url.com"))
    ->setConfigSaver(new SwooleSaver(new SingleSaver($map), "swoole_server.pid"))
    ->readConfig("appid", "cluserName", array_keys($map));

```
