<?php
declare(strict_types=1);

namespace Dreamcat\ApolloPhp;

use Dreamcat\ApolloPhp\Popo\ConfigResult;

/**
 * 配置保存接口
 * @author vijay
 */
interface ConfigSaverInterface
{
    /**
     * 保存配置
     * @param ConfigResult $configResult 配置的拉取结果
     * @return bool 是否保存成功
     */
    public function saveConfig(ConfigResult $configResult): bool;

    /**
     * 当所有配置保存完成之后调用
     * @return void
     */
    public function onSaveAll(): void;
}

# end of file
