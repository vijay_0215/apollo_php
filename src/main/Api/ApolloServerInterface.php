<?php
declare(strict_types=1);

namespace Dreamcat\ApolloPhp\Api;

use Dreamcat\ApolloPhp\Popo\ConfigResult;

/**
 * apollo服务器接口
 * @author vijay
 */
interface ApolloServerInterface
{
    /**
     * 从缓存中获取配置
     * @param string $appid 应用的appId
     * @param string $clusterName 集群名
     * @param string $namespaceName Namespace的名字
     * @param string $ip 应用部署的机器ip
     * @return array 配置内容，键是配置名，值是配置的值
     * @see https://github.com/ctripcorp/apollo/wiki/其它语言客户端接入指南#12-通过带缓存的http接口从apollo读取配置
     */
    public function getConfigFromCache(
        string $appid,
        string $clusterName,
        string $namespaceName,
        string $ip = null
    ): ?array;

    /**
     * 获取配置，不读缓存
     * @param string $appid 应用的appId
     * @param string $clusterName 集群名
     * @param string $namespaceName Namespace的名字
     * @param string $ip 应用部署的机器ip
     * @param string $releaseKey 上一次的releaseKey
     * @return ConfigResult
     * @see https://github.com/ctripcorp/apollo/wiki/其它语言客户端接入指南#13-通过不带缓存的http接口从apollo读取配置
     */
    public function getConfig(
        string $appid,
        string $clusterName,
        string $namespaceName,
        string $ip = null,
        string $releaseKey = null
    ): ConfigResult;

    /**
     * 应用感知配置更新
     * @param string $appid 应用ID
     * @param string $clusterName 集群名
     * @param array $notifications 键是namespace，值是最新的通知id
     * @return array 有变化的情况，键是namespace，值是最新的通知id
     * @see https://github.com/ctripcorp/apollo/wiki/其它语言客户端接入指南#14-应用感知配置更新
     */
    public function notifications(
        string $appid,
        string $clusterName,
        array $notifications
    ): array;
}

# end of file
