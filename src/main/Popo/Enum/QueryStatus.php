<?php
declare(strict_types=1);

namespace Dreamcat\ApolloPhp\Popo\Enum;

use MyCLabs\Enum\Enum;

/**
 * 配置查询的状态
 * @author vijay
 * @method static QueryStatus FAILED()
 * @method static QueryStatus NO_CHANGE()
 * @method static QueryStatus CHANGED()
 */
class QueryStatus extends Enum
{
    /** @var int 失败 */
    const FAILED = 0;
    /** @var int 没有变化 */
    const NO_CHANGE = 1;
    /** @var int 有变化 */
    const CHANGED = 2;
}

# end of file
