<?php
declare(strict_types=1);

namespace Dreamcat\ApolloPhp\Popo;

use Dreamcat\ApolloPhp\Popo\Enum\QueryStatus;

/**
 * 配置结果
 * @author vijay
 */
class ConfigResult
{
    /**
     * @var QueryStatus 状态
     * @AllowNull
     */
    private $queryStatus;
    /** @var string 应用ID */
    private $appId;
    /** @var string 集群名 */
    private $cluster;
    /** @var string 名空间 */
    private $namespaceName;
    /** @var array 拉到的配置，键是配置名，值是配置内容 */
    private $configurations;
    /** @var string 版本号 */
    private $releaseKey;

    /**
     * @return QueryStatus 状态
     */
    public function getQueryStatus(): QueryStatus
    {
        return $this->queryStatus;
    }

    /**
     * @param QueryStatus $queryStatus 状态
     * @return static 对象本身
     */
    public function setQueryStatus(QueryStatus $queryStatus): ConfigResult
    {
        $this->queryStatus = $queryStatus;
        return $this;
    }

    /**
     * @return string 应用ID
     */
    public function getAppId(): string
    {
        return $this->appId;
    }

    /**
     * @param string $appId 应用ID
     * @return static 对象本身
     */
    public function setAppId(string $appId): ConfigResult
    {
        $this->appId = $appId;
        return $this;
    }

    /**
     * @return string 集群名
     */
    public function getCluster(): string
    {
        return $this->cluster;
    }

    /**
     * @param string $cluster 集群名
     * @return static 对象本身
     */
    public function setCluster(string $cluster): ConfigResult
    {
        $this->cluster = $cluster;
        return $this;
    }

    /**
     * @return string 名空间
     */
    public function getNamespaceName(): string
    {
        return $this->namespaceName;
    }

    /**
     * @param string $namespaceName 名空间
     * @return static 对象本身
     */
    public function setNamespaceName(string $namespaceName): ConfigResult
    {
        $this->namespaceName = $namespaceName;
        return $this;
    }

    /**
     * @return array 拉到的配置
     */
    public function getConfigurations(): array
    {
        return $this->configurations;
    }

    /**
     * @param array $configurations 拉到的配置
     * @return static 对象本身
     */
    public function setConfigurations(array $configurations): ConfigResult
    {
        $this->configurations = $configurations;
        return $this;
    }

    /**
     * @return string 版本号
     */
    public function getReleaseKey(): ?string
    {
        return $this->releaseKey;
    }

    /**
     * @param string $releaseKey 版本号
     * @return static 对象本身
     */
    public function setReleaseKey(string $releaseKey): ConfigResult
    {
        $this->releaseKey = $releaseKey;
        return $this;
    }
}

# end of file
