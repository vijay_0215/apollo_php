<?php
declare(strict_types=1);

namespace Dreamcat\ApolloPhp;

use Dreamcat\ApolloPhp\Api\ApolloServerInterface;

/**
 * 读取apollo的进程
 * @author vijay
 */
interface ApolloReadProcessInterface
{
    /**
     * 设置服务器
     * @param ApolloServerInterface $apolloServer apollo服务器
     * @return static 对象本身
     */
    public function setApolloServer(ApolloServerInterface $apolloServer): ApolloReadProcessInterface;

    /**
     * 设置配置保存器
     * @param ConfigSaverInterface $configSaver 配置保存器
     * @return static 对象本身
     */
    public function setConfigSaver(ConfigSaverInterface $configSaver): ApolloReadProcessInterface;

    /**
     * 开始读取配置
     * @param string $appId 应用ID
     * @param string $clusterName 集群名
     * @param string[] $namespaces 要关注的名空间列表
     * @param string $ip 应用部署的机器ip
     * @return void
     */
    public function readConfig(string $appId, string $clusterName, array $namespaces, string $ip = null): void;
}

# end of file
